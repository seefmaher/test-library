package com.kordiatest.testlibrary;

import com.kordiatest.deeplibrarytest.DeepLibraryClass;

public class TestVar {
    private String testString;
    private int testInt;
    public TestVar() {
    }

    public String getTestString() {
        testString = new DeepLibraryClass().getDeepString();
        return testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }

    public int getTestInt() {
        return testInt;
    }

    public void setTestInt(int testInt) {
        this.testInt = testInt;
    }
}
